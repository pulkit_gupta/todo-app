(function (window) {
	'use strict';


	function Template() {

		this.listInner = '<div><input disabled type="text" value= "{{title}}" placeholder="Enter Task...">'
				+'<span id = "{{checkBtnID}}" class="check_icon"><i class="fa fa-check" aria-hidden="true"></i></span><i id = "{{deleteBtnID}}"  class="fa fa-trash-o" aria-hidden="true"></i><i class="fa fa-caret-left" aria-hidden="true"></i>'
				+'</div>';				
		this.defaultTemplate = '<li id = "{{id}}" class="{{done}}">'+this.listInner+'</li>';		
	}

	/**
	 * Dynamic view to add on startup from Model
	 * 
	 */
	Template.prototype.getView = function (data) {
		var i;
		var view = '';

		for (i = 0; i < data.length; i++) {
			var template = this.defaultTemplate;		
			if (data[i].completed == 1) {
				template = template.replace('{{done}}', "done");
			}else{
				template = template.replace('{{done}}', "incomplete");
			}

			template = template.replace('{{id}}', 'data-id-'+data[i].id);
			template = template.replace('{{deleteBtnID}}', 'delete-id-'+data[i].id);
						template = template.replace('{{checkBtnID}}', 'toggle-id-'+data[i].id);

			template = template.replace('{{title}}', data[i].title);
			
			view = view + template;
		}
	
		return view;
	};

	/**
	 * Used to get new Elements dynamically
	 * 
	 */
	Template.prototype.getNode = function (data) {
		var i;
		var liElem = document.createElement("LI");

			var template = this.listInner;		
			if (data.done == 1) {
				liElem.class = "done";
			}else{
			    liElem.class = "incomplete";
			}

			liElem.id = 'data-id-'+data.id

			template = template.replace('{{deleteBtnID}}', 'delete-id-'+data.id);
						template = template.replace('{{checkBtnID}}', 'toggle-id-'+data.id);

			template = template.replace('{{title}}', data.title);
			
		
		liElem.innerHTML = template;
		return liElem;
	};


	// Export to window
	window.app = window.app || {};
	window.app.Template = Template;
})(window);