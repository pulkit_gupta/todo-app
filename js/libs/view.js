
(function (window) {
	'use strict';

	/**
	 * View constructor, mainly for all the static ID DOM
	 * 
	 */
	function View(template) {
		this.template = template;

		this.$todoList = document.querySelector('.todo-list');

		this.$newTodo = document.getElementById('new-todo');	
		this.$addNewToDo = document.getElementById('new-todo-btn');	
		
	}


	/**
	 * Output from the controller in drection to View.
	 * 
	 */
	View.prototype.render = function (cmd, parameter, value) {
		var self = this;
		var viewCommands = {
			showEntries: function () {
				self.$todoList.innerHTML = self.template.getView(parameter);
			},
			addedNewTodo: function(){
				self.$newTodo.value = "";
				self.$todoList.appendChild(self.template.getNode(parameter));		

				//self.$todoList.innerHTML += self.template.getView([parameter]);								
			},
			removeItem: function(){
				var child = document.getElementById('data-id-'+parameter);
				self.$todoList.removeChild(child);
			},

			toggleItem: function(){
				var child = document.getElementById('data-id-'+parameter);

				child.className = value==1?"done": "incomplete";
			},
			markAllDone: function(){
				self.markAllDone();
			}


		};

		//console.log("values is"+ viewCmd + "and param is "+ parameter);
		viewCommands[cmd]();
	};


	/**
	 * Incomming information from the View to controller to chage Model or view again
	 * 
	 */
	View.prototype.bind = function (event, handler, id) {
		var self = this;		
		if (event === 'newTodo') {

			self.$addNewToDo.addEventListener('click',function () {
				handler(self.$newTodo.value);
			})

		}

		if(event === 'deleteTodo'){
			var eventId = 'delete-id-'+id;
			console.log("Binding event to "+ eventId);
			document.getElementById(eventId).addEventListener('click', function(event){
				var _id = event.target.id.split("-")[2];
				handler(_id);
				//delete the list with id
			});	
		}

		if(event === 'toggleTodo'){
			var eventId = 'toggle-id-'+id;
     		console.log("Binding event to "+ eventId);
			document.getElementById(eventId).addEventListener('click', function(event){
			    var _id = event.target.id.split("-")[2];
			    //console.log("id is "+_id);
				//toggle the check/uncheck
				handler(id);

			});	
		}

		if(event == "allDone"){

		}

	};

	// Export to window
	window.app = window.app || {};
	window.app.View = View;
}(window));