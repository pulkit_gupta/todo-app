(function (window) {
	'use strict';

	/**
	 * Constructor to bind all incomming events from view.
	 * todo-list
	 */
	function Controller(model, view) {
		var self = this;
		self.model = model;
		self.view = view;

		self.view.bind('newTodo', function (title) {
			self.addItem(title);

		});

		//HAVE TO BE IMPLEMENTED (NOT WORKING RIGHT NOW)
		self.view.bind('markAll', function (status) {
			self.markAll(status.completed);
		});
	}


	/**
	 * An event to fire on load. Will get all items and display them in the
	 * todo-list
	 */
	Controller.prototype.showAll = function () {
		var self = this;
		self.model.read(function (data) {
		self.view.render('showEntries', data);

		for(var i = 0 ; i < data.length; i++){

			var _id = data[i].id;
				self.view.bind('deleteTodo', function (id) {
			    	self.removeItem(id);
			    }, _id);

			    self.view.bind('toggleTodo', function (id) {
			    	self.toggleItem(id);
			    }, _id);
		}

		});
	};


	/**
	 * Routine to add the task in db and update view.
	 */
	Controller.prototype.addItem = function (title) {
		var self = this;

		if (title.trim() === '') {
			return;
		}

		self.model.create(title, function (newItem) {
			
			var _id = newItem.id;
			console.log("Saved model and now clearing the new todo view"+ _id);
		    self.view.render('addedNewTodo', newItem);

		    self.view.bind('deleteTodo', function (id) {
		    	
		    	self.removeItem(id);

		    }, _id);

		    self.view.bind('toggleTodo', function (id) {
		    	self.toggleItem(id);

		    }, _id);

		});
	};

	/**
	 *	Routine to remove the item in DB and then update the view. 
	 *
	 */
	Controller.prototype.removeItem = function (id) {
		var self = this;
		self.model.remove(id, function () {
			self.view.render('removeItem', id);
		});
	};

	/**
	 * Toggle the checked/unchecked on the task. Mainly updating the Model and later view.
	 * 
	 */
	Controller.prototype.toggleItem = function (id) {
		var self = this;

		self.model.read(id, function (data) {

			var value = data[0].completed == 0? 1 : 0;
			//save new value and update UI
			console.log("id to be send is "+ data[0].id);

			
			self.model.update(data[0].id, {completed: value}, function (newData) {
			    //update the UI	
				console.log("The done is toggled with value"+ newData.completed+ "of "+newData.id);
				self.view.render('toggleItem', newData.id, newData.completed)

			});

		});


	};

	// Export to window
	window.app = window.app || {};
	window.app.Controller = Controller;
})(window);