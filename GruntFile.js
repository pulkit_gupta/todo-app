module.exports = function(grunt){

	grunt.initConfig({

		concat: {
			css: {
			     src: ['css/reset.css', 'css/main.css'],
			     dest: 'build/main.css',
			}
		},
		watch: {
			js: {
				files: ['js/**/*.js'],
				tasks: ['uglify']
			},
			css: {
				files: ['css/**/*.css'],
				tasks: ['prepare_css']
			}
		},

		uglify: {
			options: {
				mangle: true
			},
		    js: {
		      files: {
		        'build/todo.min.js': ['js/libs/*.js' ]
		      }
		    },
		    mainjs: {
		    	files: {
		        'build/main.min.js': ['js/main.js' ]
		      }	
		    }
		},		  

		sass: {
		    dist: {
		      files: {
		        'build/main.css': 'sass/main.scss'
		      }
		    }
		},

		cssmin: {
		  options: {
		    shorthandCompacting: false,
		    roundingPrecision: -1
		  },
		  target: {
		    files: {
		      'build/main.min.css': ['build/main.css']
		    }
		  }
		}


	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('compile_scss', ['sass', 'cssmin']);

	grunt.registerTask('prepare_css', ['concat', 'cssmin']);

	grunt.registerTask('default', ['uglify', 'prepare_css', 'watch']);

}

